<!-- barra de navegación --> 
<!-- -->


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Navbar</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('facultades.index')}}">Facultades</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('programas.index')}}">Programas</a>
      </li>

    </ul>

    <!-- comprueba si el usurio autentificado -->
    @if (Auth::check())
    <ul class="navbar-nav ">
      <li class="nav-item">
        <a href="{{route('datos_personales.index')}}" class="nav-link">{{auth()->user()->name}}</a>
     </li>
     <li>
      <a
      class="nav-link" 
      href="{{ route('logout') }}"  
      onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
      Logout
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
     {{ csrf_field() }}
   </form>
 </li>		    
</ul>
@else 	
<ul class="navbar-nav ">
  <li class="nav-item">
   <a href="/login" class="nav-link ">Iniciar sesión</a>
 </li>
</ul>
@endif 

</div>
</nav>	