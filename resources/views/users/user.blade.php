@extends('layouts/layout')

@section('title', 'Titulos') <!-- titulo de la de la pestaña -->

@section('content')
<div class="container">
	<br>
	<h3>Datos personales</h3>
	<ul class="list-group list-group-flush">
		<h5 class="list-group-item">Nombre</h5>
		<li class="list-group-item">{{$user->name}}</li>
		<h5 class="list-group-item">Dirección</h5>
		<li class="list-group-item">{{$user->direccion}}</li>
		<h5 class="list-group-item">Email</h5>
		<li class="list-group-item">{{$user->email}}</li>
		<h5 class="list-group-item">Telefono</h5>
		<li class="list-group-item">{{$user->telefono}}</li>
		<h5 class="list-group-item">Ciudad de residencia</h5>
		<li class="list-group-item">{{$user->ciudad_residencia}}</li>
		<h5 class="list-group-item">Ciudad de origen</h5>
		<li class="list-group-item">{{$user->ciudad_origen}}</li>
		<h5 class="list-group-item">Nacionalidad</h5>
		<li class="list-group-item">{{$user->ciudad_nacionalidad}}</li>

	</ul>
</div>
	
	
@endsection