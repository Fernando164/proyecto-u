@extends('..layouts/layout')

@section('title', 'Modificar facultades') <!-- titulo de la de la pestaña -->

@section('content')


<div class="container justify-content-md-center">	
	<br>

	<!-- Vista de la facultad -->
	<div >
		<h1>{{$faculty->name}}</h1>
		<p class="text-justify"> {{$faculty->description}} </p>
		<form method="post" action= "{{route('facultades.destroy', $faculty->id)}}" >
			{{csrf_field()}}
			<input type="hidden" name="_method" value="DELETE">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>
			<button class="btn btn-secondary"  type="submit" name="send" value="Eliminar" >Eliminar</button>
		</form>
	</div>

	<br>

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">


			<div class="modal-content">
				<div class="modal-header">

					<h4 class="modal-title">Editar</h4>
				</div>
				<div class="modal-body">
					<div >
						<!-- formulario para enviar datos actualizados de una facultad ya existente. Los datos se envían a la misma ruta y los toma la public function edit del controllador (facultiesController), para actualizar  registros en la tabla faculties-->
						<form method="post" action= "{{route('facultades.update', $faculty->id)}}">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="PUT">
							<div class="mb-3"> 
								<input type="text" class="form-control"  name="name" value="{{$faculty->name}}" required autofocus>
							</div>
							<div class="mb-3"> 
								<textarea class="form-control" id="description" name="description" ></textarea> 
							</div>
							<button type="submit" class="btn btn-primary" name="send">Aceptar</button>
						</form>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>


	<h3>Programas vinculados</h3>
	<ul class="list-group list-group-flush">

		<!-- comprueba si el array  esta vacio-->
		@if($programs == '[]')
		<br>
		<p>No hay programas vinculados a esta facultad <a href="{{route('programas.index')}}/" class="link-light">ir</p> 
		@else
		@foreach($programs as $program)

		<li class="list-group-item"><a href="{{route('programas.show', $program->id)}}/" class="link-light">{{$program->name}}</a></li>

		@endforeach
		@endif 

	</ul> 
	<br>
</div>


@endsection