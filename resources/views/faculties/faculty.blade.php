<!-- vista principal de facultades, incluida desde la public function index en el controllador facultiesController-->

@extends('layouts/layout')

@section('title', 'Facultades') <!-- titulo de la de la pestaña -->

@section('content')

	<!-- formulario para crear nuevas facultades. Los datos se envían a la misma ruta y los toma la public function store del controllador (facultiesController) para crear nuevos registros en la tabla faculties-->

<div class="container">	
	<br>
	<h3>Crear facultades</h3>
	<form method="post" action= " {{route('facultades.store')}}">
		{{csrf_field()}}
		<div class="mb-3">
		    <input type="text" class="form-control"  name="name" placeholder ="Nombre..." required autofocus>
		</div>
		<div class="mb-3">
		    <label for="description"  class="form-label" >Descripción</label>
		    <textarea class="form-control" id="description" name="description"></textarea> 
	    </div>
		<button type="submit" class="btn btn-primary" name="send">Crear</button>
	</form>
</div>

<br>
	
<!-- este foreach muestra los datos de la tabla faculties--> 	
<div class="container">
	<table class="table table-striped table-responsive" >
	  <thead class="thead-dark">
	    <tr >
	      <th scope="col">Nombre</th>
	      <th scope="col">Descripción</th>
	    </tr>
	  </thead>	
	  <tbody>
		@foreach ($faculties as $faculty)
		<tr>
	      <td><a class="link-light" href="{{route('facultades.show', $faculty->id)}}">{{$faculty->name}}</a></td>
	      <td>{{$faculty->description}}</td>
	    </tr>
		@endforeach
  </tbody>
</table>
	
@endsection