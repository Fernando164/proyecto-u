@extends('..layouts/layout')

@section('title', '') <!-- titulo de la de la pestaña -->

@section('content')



<!-- formulario para enviar datos actualizados de una facultad ya existente. Los datos se envían a la misma ruta y los toma la public function edit del controllador (facultiesController), para actualizar  registros en la tabla faculties-->


<!-- Boton para enviar el id de la facultad y eliminarla desde public function edit en el controllador (facultiesController), de la tabla faculties --> 


<div class="container justify-content-md-center">
	<br>

	<!-- Vista de la facultad -->
	<div >
		<h3>{{$program->name}}</h3>
		<p class="text-justify"> {{$program->description}} </p>
		<form method="post" action= "{{ route('programas.index')}}/{{$program->id}}" >
			{{csrf_field()}}
			<!-- Boton eliminar el registro de la tabla-->		
			<input type="hidden" name="_method" value="DELETE">

			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>
			<button class="btn btn-secondary"  type="submit" name="send" value="Eliminar" >Eliminar</button>
		</form>
	</div>

	<br>

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">


			<div class="modal-content">
				<div class="modal-header">

					<h4 class="modal-title">Editar</h4>
				</div>
				<div class="modal-body">
					<div >
						<!-- formulario para enviar datos actualizados de un programa ya existente. Los datos se envían a la misma ruta y los toma la public function update del controllador (programsController), para actualizar  registros en la tabla programs-->
						<form method="post" action= "{{ route('programas.index')}}/{{$program->id}}">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="PUT">
							<div class="mb-3"> 
								<input type="text" class="form-control"  name="name" value="{{$program->name}}" required autofocus>
							</div>
							<select name="faculty_id">
								@foreach($faculties as $faculty)
								<option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
								@endforeach
							</select>
							<div class="mb-3"> 
								<textarea class="form-control" id="description" name="description" ></textarea> 
							</div>
							<button type="submit" class="btn btn-primary" name="send" value="Enviar">Aceptar</button>
						</form>


					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>



	<!-- Boton para enviar el id de la facultad y eliminarla desde public function edit en el controllador (facultiesController), de la tabla faculties --> 

</div>

@endsection


