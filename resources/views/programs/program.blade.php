@extends('layouts/layout')

@section('title', 'Programas') <!-- titulo de la de la pestaña -->

@section('content')


<!-- formulario para crear nuevos programas. Los datos se envían a la misma ruta y los toma la public function store del controllador (programsController) para crear nuevos registros en la tabla programs-->

<div class="container">
	<br>

@if(count($faculties) >= 1)
	<h3>Crear programas</h3>
	<form method="post" action= " {{route('programas.store')}}">
		{{csrf_field()}}
		<div class="mb-3">
			<input type="text" class="form-control"  name="name" placeholder ="Nombre..." required autofocus>
		</div>
		<select name="faculty_id">
			@foreach($faculties as $faculty)
			<option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
			@endforeach
		</select>
		<div class="mb-3">
			<label for="description"  class="form-label" >Descripción</label>
			<textarea class="form-control" id="description" name="description"></textarea> 
		</div>
		<button type="submit" class="btn btn-primary" name="send">Crear</button>
	</form>

@else 
	<h3>Crea facultades para poder administrar programas<a class="link-light" href="{{route('facultades.index')}}"> ir</a></h3>
@endif


<br>

<!-- este foreach muestra los datos de la tabla faculties--> 	
	<table class="table table-striped table-responsive" >
		<thead class="thead-dark">
			<tr >
				<th scope="col">Nombre</th>
				<th scope="col">Descripción</th>
				<th>	</th>
			</tr>
		</thead>	
		<tbody>
			@foreach ($programs as $program)
			<tr>
				<td><a class="link-light" href="{{route('programas.show', $program->id)}}">{{$program->name}}</a></td>
				<td>{{$program->description}}</td>
				<td>

					 @if($student != '[]')
					 	Registrado

					 @else

					 	<form method="post" action= " {{route('datos_personales.store')}}">
						{{csrf_field()}}
						<div class="mb-3">
							<input type="hidden" name="program_id" value="{{$program->id}}">
						</div>
						<button type="submit" class="btn btn-success" name="send">Unirse</button>
					</form>

					 @endif

					

				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
	
	@endsection