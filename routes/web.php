<?php

use App\Faculty;
use App\Program;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*ruta para la pagina de inicio"*/
/*Route::get('/', function(){
	$user = Auth::user();

	return $user;
});*/


Route::resource('/facultades', 'facultiesController');
Route::resource('/programas', 'programsController');
Route::resource('/datos_personales', 'usersController');




Auth::routes();

Route::get('/', 'HomeController@index')->name('home');



