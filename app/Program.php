<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{

	protected $fillable=[

    	"name",
    	"faculty_id",
    	"description"

    ];

    public function faculty(){

    	return $this->belongsTo("App\Faculty");

    }
    
}
