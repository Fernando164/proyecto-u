<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Faculty;
use App\Program;

class facultiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (Auth::check()) {

        //consulta con el modelo Faculty a la tabla faculties 
            $faculties = Faculty::all();

        //retorno de la vista junto con los datos de la consulta
            return view('faculties/faculty', compact('faculties')); 
        } else {
            return redirect("/");

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return "404";
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        /*Recibe los datos del formulario en la ruta facultades para crear datos en la tabla -> faculties*/ 
        $faculty = new Faculty;
        $faculty->name = $request->name;
        $faculty->description = $request->description;
        $faculty->save();

        return redirect("/facultades");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        /*Hace una consulta con la id que se proporciona en la ruta facultades/{id}*/
        $faculty =  Faculty::find($id);
        //return $programs =  Faculty::find($id)->programs;
        $programs =  Program::where('faculty_id', $id)->get();

        //vista 
        return view('faculties/show', compact('faculty', 'programs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faculty = Faculty::find($id);

        $faculty->update($request->all());

        return redirect("/facultades/{$id}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faculty = Faculty::find($id);

        $faculty->delete();

        return redirect("/facultades"); 
    }
}
