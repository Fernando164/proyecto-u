<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=[

    	"program_id",
    	"user_id"

    ];
}
